FROM docker:stable

RUN apk update && apk add --virtual .build-deps build-base python3-dev libressl-dev musl-dev libffi-dev \
  && apk add --no-cache python3 git zip \
  && pip3 install --upgrade pip setuptools \
  && if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi \
  && if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi \
  && pip install awscli \
  && pip install awsebcli \
  && apk del .build-deps \
  && rm -rf /root/.cache \
  && rm -rf /var/cache/apk/*
